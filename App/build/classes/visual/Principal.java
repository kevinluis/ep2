package visual;

import app.Carreta;
import app.Carro;
import app.Moto;
import app.Van;
import app.Veiculos;


public class Principal extends javax.swing.JFrame {

    public Principal() {
        initComponents();
    }
    public static Veiculos veiculo = new Veiculos();
    public static Carro carro = new Carro();
    public static Moto moto = new Moto();
    public static Van van = new Van();
    public static Carreta carreta = new Carreta();

    public static int nCarros = carro.nCarros();
    public static int nCarretas = carreta.nCarretas();
    public static int nMotos = moto.nMotos();
    public static int nVans = van.nVans();
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        registro = new javax.swing.JButton();
        lucro = new javax.swing.JButton();
        frete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel1.setText("KL Transportadora");

        registro.setText("Registro de frota");
        registro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registroActionPerformed(evt);
            }
        });

        lucro.setText("Alterar margem de lucro");
        lucro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lucroActionPerformed(evt);
            }
        });

        frete.setText("Calcular frete");
        frete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                freteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(registro)
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(lucro))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(137, 137, 137)
                        .addComponent(frete)))
                .addContainerGap(115, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addGap(49, 49, 49)
                .addComponent(registro)
                .addGap(28, 28, 28)
                .addComponent(lucro)
                .addGap(29, 29, 29)
                .addComponent(frete)
                .addContainerGap(57, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void freteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_freteActionPerformed
        CalcularFrete calcular = new CalcularFrete();
        calcular.setVisible(true);
        this.dispose();
        calcular.setPanelInvisivel();
    }//GEN-LAST:event_freteActionPerformed

    private void registroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registroActionPerformed
        JanelaRegistro janelaregistro = new JanelaRegistro();
        janelaregistro.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_registroActionPerformed

    private void lucroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lucroActionPerformed
        MargemLucro margemlucro = new MargemLucro();
        margemlucro.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lucroActionPerformed

    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton frete;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton lucro;
    private javax.swing.JButton registro;
    // End of variables declaration//GEN-END:variables
}
