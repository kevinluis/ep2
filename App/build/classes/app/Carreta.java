package app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public final class Carreta extends Veiculos {

    private int Numero_de_carretas;
    private float VelocidadeMedia;
    private float PesoMaximo;

    public Carreta() {
        setVelocidadeMedia(60);
        setPesoMaximo(30000);
    }

    public float getVelocidadeMedia() {
        return VelocidadeMedia;
    }

    public void setVelocidadeMedia(float VelocidadeMedia) {
        this.VelocidadeMedia = VelocidadeMedia;
    }

    public float getPesoMaximo() {
        return PesoMaximo;
    }

    public void setPesoMaximo(float PesoMaximo) {
        this.PesoMaximo = PesoMaximo;
    }

    public void setNumero_de_carretas(int Numero_de_carretas) {
        this.Numero_de_carretas = Numero_de_carretas;
    }

    public int getNumero_de_carretas() {
        return Numero_de_carretas;
    }

    @Override
    public float Calcula_lucrot_diesel(float Peso, float km) {
        Veiculos v = new Veiculos();
        v.setMargemLucro(v.lerLucro());

        float Rendimento = (float) ((float) 8.0 - (Peso * 0.0002));
        float taxaLucro = (float) ((v.getMargemLucro() / 100) + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 3.869 * taxaLucro);

        return lucro;

    }

    @Override
    public float Tempo_de_viagem(float Km) {

        float Tempo_de_viagem = Km / getVelocidadeMedia();

        return Tempo_de_viagem;

    }

    public int nCarretas() {
        Carreta c = new Carreta();
        int n;
        try {
            FileInputStream arquivo = new FileInputStream("src/app/carreta.txt");
            InputStreamReader input = new InputStreamReader(arquivo);
            BufferedReader br = new BufferedReader(input);

            String linha;

            do {
                linha = br.readLine();
                if (linha != null) {
                    c.setNumero_de_carretas(Integer.parseInt(linha));
                }
            } while (linha != null);

        } catch (Exception e) {
            System.out.println("error");
        }
        n = c.getNumero_de_carretas();
        return n;

    }

    public void alterarNCarretas(int n) {
        String N = String.valueOf(n);
        try {
            FileOutputStream arquivo = new FileOutputStream("src/app/carreta.txt");
            PrintWriter pr = new PrintWriter(arquivo);

            pr.println(N);

            pr.close();
            arquivo.close();
        } catch (Exception e) {
            System.out.println("ERROR");
        }

    }

    @Override
    public boolean RealizaEntrega(float peso, float tmaximo, float distancia, int quantidadeVeiculos) {

        if (Tempo_de_viagem(distancia) <= tmaximo && peso <= getPesoMaximo() && 0 < quantidadeVeiculos) {
            return true;
        } else {
            return false;
        }

    }
    public String MostraConfirmacao(){
        
        String s1 = "Voce Selecionou uma carreta";

        return s1;
    }
}
