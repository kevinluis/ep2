/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;

import app.Carreta;
import app.Carro;
import app.Moto;
import app.Van;
import app.Veiculos;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import static visual.Principal.nCarretas;
import static visual.Principal.nCarros;
import static visual.Principal.nMotos;
import static visual.Principal.nVans;

public class CalcularFrete extends javax.swing.JFrame {

    /**
     * Creates new form CalcularFrete
     */
    public CalcularFrete() {
        initComponents();
    }

    public static Veiculos v1 = new Veiculos();
    public static Carro c1 = new Carro();
    public static Carreta c2 = new Carreta();
    public static Moto m = new Moto();
    public static Van v2 = new Van();
    public static Format f = new DecimalFormat("0.00");
    public static String s1, s2; // Strings para função do menor tempo
    public static String s3, s4; // Strings para função do menor custo
    public static String s5, s6; // Strings para função do menor custo
    public static float h;
    public int i;
    public boolean a1, b1, a2, b2;

    public int nC1 = nCarros, nC2 = nCarretas, nV2 = nVans, nM = nMotos;
    public String C1 = "Nº de carros disponíveis: " + String.valueOf(nC1);
    public String C2 = "Nº de carretas disponíveis: " + String.valueOf(nC2);
    public String V2 = "Nº de vans disponíveis: " + String.valueOf(nV2);
    public String M = "Nº de motos disponíveis: " + String.valueOf(nM);

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        peso = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tmaximo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        distancia = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel1.setText("Peso(kg):");

        peso.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        peso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pesoActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel2.setText("Tempo Máximo(h):");

        tmaximo.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel3.setText("Distancia(km):");

        distancia.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N

        jButton1.setText("Calcular");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 164, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel4)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 154, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 77, Short.MAX_VALUE)
                    .addComponent(jLabel4)
                    .addGap(0, 77, Short.MAX_VALUE)))
        );

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 164, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel5)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 154, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 77, Short.MAX_VALUE)
                    .addComponent(jLabel5)
                    .addGap(0, 77, Short.MAX_VALUE)))
        );

        jLabel7.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 190, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 95, Short.MAX_VALUE)
                    .addComponent(jLabel7)
                    .addGap(0, 95, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 154, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 77, Short.MAX_VALUE)
                    .addComponent(jLabel7)
                    .addGap(0, 77, Short.MAX_VALUE)))
        );

        jLabel6.setText(C1);

        jLabel8.setText(C2);

        jLabel9.setText(V2);

        jLabel10.setText(M);

        jButton2.setText("Voltar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Recalcular");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Escolher carro");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Escolher carreta");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("Escolher van");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("Escolher moto");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tmaximo, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(distancia, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel10)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton4)
                            .addComponent(jButton5)
                            .addComponent(jButton6)
                            .addComponent(jButton7)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(67, 67, 67)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(281, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tmaximo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jButton5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(distancia, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jButton6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jLabel10)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(165, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void pesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pesoActionPerformed
    }//GEN-LAST:event_pesoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (Float.parseFloat(peso.getText()) > 30000) {
            JOptionPane.showMessageDialog(null, "Error");
        } else {
            jPanel1.setVisible(true);
            jPanel2.setVisible(true);
            jPanel3.setVisible(true);
            jButton3.setVisible(true);
            jLabel6.setVisible(true);
            jLabel8.setVisible(true);
            jLabel9.setVisible(true);
            jLabel10.setVisible(true);
            s2 = EntregaMaisRapida();
            jLabel4.setText(s2);
            s4 = EntregaMenorGusto();
            jLabel5.setText(s4);
            s6 = EntregaCustoBeneficio();
            jLabel7.setText(s6);
            mostraButton4();
            mostraButton5();
            mostraButton6();
            mostraButton7();

        }


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        this.dispose();
        CalcularFrete calc = new CalcularFrete();
        calc.setVisible(true);
        calc.setPanelInvisivel();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        setPanelInvisivel();
        Principal p = new Principal();
        p.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        s1 = c1.MostraConfirmacao();
        JOptionPane.showMessageDialog(null, s1);
        setPanelInvisivel();
        nCarros--;
        Principal p = new Principal();
        p.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        s1 = c2.MostraConfirmacao();
        JOptionPane.showMessageDialog(null, s1);
        setPanelInvisivel();
        nCarretas--;
        Principal p = new Principal();
        p.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        s1 = v2.MostraConfirmacao();
        JOptionPane.showMessageDialog(null, s1);
        setPanelInvisivel();
        nVans--;
        Principal p = new Principal();
        p.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        s1 = m.MostraConfirmacao();
        JOptionPane.showMessageDialog(null, s1);
        setPanelInvisivel();
        nMotos--;
        Principal p = new Principal();
        p.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton7ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalcularFrete().setVisible(true);
            }
        });
    }

    public void setPanelInvisivel() {
        jPanel1.setVisible(false);
        jPanel2.setVisible(false);
        jPanel3.setVisible(false);

        jButton3.setVisible(false);
        jButton4.setVisible(false);
        jButton5.setVisible(false);
        jButton6.setVisible(false);
        jButton7.setVisible(false);

        jLabel6.setVisible(false);
        jLabel8.setVisible(false);
        jLabel9.setVisible(false);
        jLabel10.setVisible(false);

    }

    public void mostraButton4() {
        if (c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1)) {
            jButton4.setVisible(true);
        } else {
            jButton4.setVisible(false);
        }
    }

    public void mostraButton5() {
        if (c2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC2)) {
            jButton5.setVisible(true);
        } else {
            jButton5.setVisible(false);
        }
    }

    public void mostraButton6() {
        if (v2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nV2)) {
            jButton6.setVisible(true);
        } else {
            jButton6.setVisible(false);
        }
    }

    public void mostraButton7() {
        if (m.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nM)) {
            jButton7.setVisible(true);
        } else {
            jButton7.setVisible(false);
        }
    }

/*    public String mostraCarro() {
        if (c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1)) {
            float hora = c1.Tempo_de_viagem(Float.parseFloat(peso.getText()));
            float precoOperacaoAlcool = c1.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            float precoOperacaoGasolina = c1.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            float custoOperacaoAlcool = (float) (precoOperacaoAlcool / (1 + (c1.lerLucro() * 0.01)));
            float custoOperacaoGasolina = (float) (precoOperacaoGasolina / (1 + (c1.lerLucro() * 0.01)));
            float lucroAlcool = precoOperacaoAlcool - custoOperacaoAlcool;
            float lucroGasolina = precoOperacaoGasolina - precoOperacaoGasolina;

            if (precoOperacaoAlcool < precoOperacaoGasolina) {
                s1 = "<html>Carro<br> </hmtl>";
            } else {
            }
        }
        String s1 = "";
        return s1;
    }*/

    public String EntregaMaisRapida() {
        ArrayList<Float> maisRapida = new ArrayList<>();
        float a, g, d, pg, pa, pd, la, lg, ld;
        if (c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1)) {
            maisRapida.add(c1.Tempo_de_viagem(Float.parseFloat(distancia.getText())));

        }
        if (c2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC2)) {
            maisRapida.add(c2.Tempo_de_viagem(Float.parseFloat(distancia.getText())));

        }
        if (v2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nV2)) {
            maisRapida.add(v2.Tempo_de_viagem(Float.parseFloat(distancia.getText())));

        }
        if (m.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nM)) {
            maisRapida.add(m.Tempo_de_viagem(Float.parseFloat(distancia.getText())));

        }

        a1 = m.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nM);
        a2 = v2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nV2);
        b1 = c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1);
        b2 = c2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC2);

        if (a1 == false && a2 == false && b1 == false && b2 == false) {
            s1 = "<html>Menor tempo:<br>Não é possivel realizar a entrega</html>";
            return s1;
        }

        maisRapida.sort(null);

        if (maisRapida.get(0) == c1.Tempo_de_viagem(Float.parseFloat(distancia.getText()))) {
            a = c1.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            g = c1.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pg = (float) (g / (1 + (c1.lerLucro() * 0.01)));
            pa = (float) (a / (1 + (c1.lerLucro() * 0.01)));

            la = a - pa;
            lg = g - pg;

            h = c1.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            if (a < g) {
                s1 = "<html>Entrega Mais Rapida<br>" + "Veiculo: Carro<br>" + "Combustivel: Álcool<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pa) + "$<br>" + "Lucro da empresa: " + f.format(la) + "$<br>" + "Preço da operação: " + f.format(a) + "$</html>";
                return s1;
            } else {
                s1 = "<html>Entrega Mais Rapida<br>" + "Veiculo: Carro<br>" + "Combustivel: Gasolina<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pg) + "$<br>" + "Lucro da empresa: " + f.format(lg) + "$<br>" + "Custo da operação: " + f.format(g) + "$</html>";
                return s1;
            }
        } else if (maisRapida.get(0) == c2.Tempo_de_viagem(Float.parseFloat(distancia.getText()))) {
            d = c2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pd = (float) (d / (1 + (c2.lerLucro() * 0.01)));

            ld = d - pd;

            h = c2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s1 = "<html>Entrega mais rapida<br>" + "Veículo: Carreta<br>" + "Combustível: Diesel<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pd) + "$<br>" + "Lucro da empresa: " + f.format(ld) + "$<br>" + "Custo da operação: " + f.format(d) + "$</html>";

            return s1;
        } else if (maisRapida.get(0) == v2.Tempo_de_viagem(Float.parseFloat(distancia.getText()))) {
            d = v2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pd = (float) (d / (1 + (v2.lerLucro() * 0.01)));

            ld = d - pd;

            h = v2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s1 = "<html>Entrega mais rapida<br>" + "Veículo: Van<br>" + "Combustível: Diesel<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pd) + "$<br>" + "Lucro da empresa: " + f.format(ld) + "$<br>" + "Custo da operação: " + f.format(d) + "$</html>";

            return s1;
        } else if (maisRapida.get(0) == m.Tempo_de_viagem(Float.parseFloat(distancia.getText()))) {
            a = m.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            g = m.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pg = (float) (g / (1 + (m.lerLucro() * 0.01)));
            pa = (float) (a / (1 + (m.lerLucro() * 0.01)));

            la = a - pa;
            lg = g - pg;

            h = m.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
            if (a < g) {
                s1 = "<html>Entrega mais rapida<br>" + "Veículo: Moto<br>" + "Combustivel: Álcool<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pa) + "$<br>" + "Lucro da empresa: " + f.format(la) + "$<br>" + "Preço da operação: " + f.format(a) + "$</html>";
                return s1;
            } else {
                s1 = "<html>Entrega mais rapida<br>" + "Veículo: Moto<br>" + "Combustivel: Gasolina<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pg) + "$<br>" + "Lucro da empresa: " + f.format(lg) + "$<br>" + "Custo da operação: " + f.format(g) + "$</html>";
                return s1;
            }

        } else {
            s1 = "<html>Não é possivel realizar a entrega</html>";
            return s1;
        }

    }

    public String EntregaMenorGusto() {
        ArrayList<Float> menorGasto = new ArrayList<>();
        float a, g, d, pg, pa, pd, la, lg, ld;
        float custot1, custot2, custot3, custot4, custot5, custot6;
        if (c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1)) {
            a = c1.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            g = c1.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            pg = (float) (g / (1 + (c1.lerLucro() * 0.01)));
            pa = (float) (a / (1 + (c1.lerLucro() * 0.01)));
            if (pa < pg) {
                menorGasto.add(pa);

            } else {
                menorGasto.add(pg);
            }
        }
        if (c2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC2)) {
            d = c2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            pd = (float) (d / (1 + (c2.lerLucro() * 0.01)));
            menorGasto.add(pd);

        }
        if (v2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nV2)) {
            d = v2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            pd = (float) (d / (1 + (c2.lerLucro() * 0.01)));
            menorGasto.add(pd);
        }
        if (m.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nM)) {
            a = m.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            g = m.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            pg = (float) (g / (1 + (c1.lerLucro() * 0.01)));
            pa = (float) (a / (1 + (c1.lerLucro() * 0.01)));
            if (pa < pg) {
                menorGasto.add(pa);
            } else {
                menorGasto.add(pg);
            }
        }

        a1 = m.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nM);
        a2 = v2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nV2);
        b1 = c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1);
        b2 = c2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC2);

        if (a1 == false && a2 == false && b1 == false && b2 == false) {
            s3 = "<html>Menor Gasto:<br>Não é possivel realizar a entrega </html>";
            return s3;
        }

        menorGasto.sort(null);
        custot1 = (float) ((c1.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()))) / (1 + (v1.lerLucro() * 0.01)));
        custot2 = (float) ((c1.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()))) / (1 + (v1.lerLucro() * 0.01)));
        custot3 = (float) ((c2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()))) / (1 + (v1.lerLucro() * 0.01)));
        custot4 = (float) ((v2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()))) / (1 + (v1.lerLucro() * 0.01)));
        custot5 = (float) ((m.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()))) / (1 + (v1.lerLucro() * 0.01)));
        custot6 = (float) ((m.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()))) / (1 + (v1.lerLucro() * 0.01)));

        if (menorGasto.get(0) == custot1) {
            a = c1.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pa = (float) (a / (1 + (c1.lerLucro() * 0.01)));

            la = a - pa;

            h = c1.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s3 = "<html>Entrega com menor custo<br>" + "Veiculo: Carro<br>" + "Combustivel: Álcool<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pa) + "$<br>" + "Lucro da empresa: " + f.format(la) + "$<br>" + "Preço da operação: " + f.format(a) + "$</html>";

            return s3;

        } else if (menorGasto.get(0) == custot2) {
            g = c1.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pg = (float) (g / (1 + (c1.lerLucro() * 0.01)));

            lg = g - pg;

            h = c1.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s3 = "<html>Entrega com menor custo<br>" + "Veiculo: Carro<br>" + "Combustivel: Gasolina<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pg) + "$<br>" + "Lucro da empresa: " + f.format(lg) + "$<br>" + "Custo da operação: " + f.format(g) + "$</html>";

            return s3;

        } else if (menorGasto.get(0) == custot3) {
            d = c2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pd = (float) (d / (1 + (c2.lerLucro() * 0.01)));

            ld = d - pd;

            h = c2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s3 = "<html>Entrega com menor custo<br>" + "Veículo: Carreta<br>" + "Combustível: Diesel<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pd) + "$<br>" + "Lucro da empresa: " + f.format(ld) + "$<br>" + "Custo da operação: " + f.format(d) + "$</html>";

            return s3;
        } else if (menorGasto.get(0) == custot4) {
            d = v2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pd = (float) (d / (1 + (c2.lerLucro() * 0.01)));

            ld = d - pd;

            h = v2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s3 = "<html>Entrega com menor custo<br>" + "Veículo: Van<br>" + "Combustível: Diesel<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pd) + "$<br>" + "Lucro da empresa: " + f.format(ld) + "$<br>" + "Custo da operação: " + f.format(d) + "$</html>";

            return s3;
        } else if (menorGasto.get(0) == custot5) {
            a = m.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pa = (float) (a / (1 + (c1.lerLucro() * 0.01)));

            la = a - pa;

            h = m.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s3 = "<html>Entrega com menor custo<br>" + "Veículo: Moto<br>" + "Combustivel: Álcool<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pa) + "$<br>" + "Lucro da empresa: " + f.format(la) + "$<br>" + "Preço da operação: " + f.format(a) + "$</html>";
            return s3;

        } else if (menorGasto.get(0) == custot6) {
            g = m.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

            pg = (float) (g / (1 + (c1.lerLucro() * 0.01)));

            lg = g - pg;

            h = m.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

            s3 = "<html>Entrega com menor custo<br>" + "Veículo: Moto<br>" + "Combustivel: Gasolina<br>" + "Tempo total: " + f.format(h) + "h<br>" + "Gastos com combustível: " + f.format(pg) + "$<br>" + "Lucro da empresa: " + f.format(lg) + "$<br>" + "Custo da operação: " + f.format(g) + "$</html>";

            return s3;
        } else {
            s3 = "<html>Não é possivel realizar a entrega</html>";
            return s3;
        }
    }

    public String EntregaCustoBeneficio() {
        ArrayList<Float> custoBeneficio = new ArrayList<>();
        float a, g, d, pg, pa, pd, la, lg, ld, cba, cbg, cbd, time;
        int size;
        float custot1, custot2, custot3, custot4, custot5, custot6;
        float time1, time2, time3, time4;
        float cba1, cba2, cbg1, cbg2, cbd1, cbd2;

        if (c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1)) {
            a = c1.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            g = c1.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            time = c1.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
            cba = a / time;
            cbg = g / time;

            if (cba < cbg) {
                custoBeneficio.add(cbg);
            } else {
                custoBeneficio.add(cba);

            }
        }
        if (c2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC2)) {
            d = c2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            time = c2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
            cbd = d / time;
            custoBeneficio.add(cbd);

        }
        if (v2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nV2)) {
            d = v2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            time = v2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
            cbd = d / time;
            custoBeneficio.add(cbd);
        }
        if (m.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nM)) {
            a = m.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            g = m.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
            time = m.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
            cba = a / time;
            cbg = g / time;

            if (cba < cbg) {
                custoBeneficio.add(cbg);
            } else {
                custoBeneficio.add(cba);
            }
        }
        a1 = m.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nM);
        a2 = v2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nV2);
        b1 = c1.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC1);
        b2 = c2.RealizaEntrega(Float.parseFloat(peso.getText()), Float.parseFloat(tmaximo.getText()), Float.parseFloat(distancia.getText()), nC2);

        if (a1 == false && a2 == false && b1 == false && b2 == false) {
            s5 = "<html>Custo Beneficio:<br>Não é possivel realizar a entrega</html>";
            return s5;
        }

        custoBeneficio.sort(null);
        size = custoBeneficio.size() - 1;

        custot1 = c1.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
        custot2 = c1.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
        custot3 = c2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
        custot4 = v2.Calcula_lucrot_diesel(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
        custot5 = m.Calcula_lucrot_alcool(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));
        custot6 = m.Calcula_lucrot_gasolina(Float.parseFloat(peso.getText()), Float.parseFloat(distancia.getText()));

        time1 = c1.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
        time2 = c2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
        time3 = v2.Tempo_de_viagem(Float.parseFloat(distancia.getText()));
        time4 = m.Tempo_de_viagem(Float.parseFloat(distancia.getText()));

        cba1 = custot1 / time1;
        cbg1 = custot2 / time1;
        cbd1 = custot3 / time2;
        cbd2 = custot4 / time3;
        cba2 = custot5 / time4;
        cbg2 = custot6 / time4;

        if (custoBeneficio.get(size) == cba1) {
            a = custot1;
            pa = (float) (a / (1 + (c1.lerLucro() * 0.01)));
            la = a - pa;
            s5 = "<html>Entrega com melhor custo beneficio<br>" + "Veiculo: Carro<br>" + "Combustivel: Álcool<br>" + "Tempo total: " + f.format(time1) + "h<br>" + "Gastos com combustível: " + f.format(pa) + "$<br>" + "Lucro da empresa: " + f.format(la) + "$<br>" + "Preço da operação: " + f.format(a) + "$</html>";
            return s5;
        } else if (custoBeneficio.get(size) == cbg1) {
            g = custot2;
            pg = (float) (g / (1 + (c1.lerLucro() * 0.01)));
            lg = g - pg;
            s5 = "<html>Entrega com melhor custo beneficio<br>" + "Veiculo: Carro<br>" + "Combustivel: Gasolina<br>" + "Tempo total: " + f.format(time1) + "h<br>" + "Gastos com combustível: " + f.format(pg) + "$<br>" + "Lucro da empresa: " + f.format(lg) + "$<br>" + "Custo da operação: " + f.format(g) + "$</html>";
            return s5;
        } else if (custoBeneficio.get(size) == cbd1) {
            d = custot3;
            pd = (float) (d / (1 + (c2.lerLucro() * 0.01)));
            ld = d - pd;
            s5 = "<html>Entrega com melhor custo beneficio<br>" + "Veículo: Carreta<br>" + "Combustível: Diesel<br>" + "Tempo total: " + f.format(time2) + "h<br>" + "Gastos com combustível: " + f.format(pd) + "$<br>" + "Lucro da empresa: " + f.format(ld) + "$<br>" + "Custo da operação: " + f.format(d) + "$</html>";
            return s5;
        } else if (custoBeneficio.get(size) == cbd2) {
            d = custot4;
            pd = (float) (d / (1 + (v2.lerLucro() * 0.01)));
            ld = d - pd;
            s5 = "<html>Entrega com melhor custo beneficio<br>" + "Veículo: Van<br>" + "Combustível: Diesel<br>" + "Tempo total: " + f.format(time3) + "h<br>" + "Gastos com combustível: " + f.format(pd) + "$<br>" + "Lucro da empresa: " + f.format(ld) + "$<br>" + "Custo da operação: " + f.format(d) + "$</html>";
            return s5;
        } else if (custoBeneficio.get(size) == cba2) {
            a = custot5;
            pa = (float) (a / (1 + (m.lerLucro() * 0.01)));
            la = a - pa;
            s5 = "<html>Entrega com melhor custo beneficio<br>" + "Veículo: Moto<br>" + "Combustivel: Álcool<br>" + "Tempo total: " + f.format(time4) + "h<br>" + "Gastos com combustível: " + f.format(pa) + "$<br>" + "Lucro da empresa: " + f.format(la) + "$<br>" + "Preço da operação: " + f.format(a) + "$</html>";
            return s5;
        } else if (custoBeneficio.get(size) == cbg2) {
            g = custot6;
            pg = (float) (g / (1 + (m.lerLucro() * 0.01)));
            lg = g - pg;
            s5 = "<html>Entrega com melhor custo beneficio<br>" + "Veículo: Moto<br>" + "Combustivel: Álcool<br>" + "Tempo total: " + f.format(time4) + "h<br>" + "Gastos com combustível: " + f.format(pg) + "$<br>" + "Lucro da empresa: " + f.format(lg) + "$<br>" + "Preço da operação: " + f.format(g) + "$</html>";
            return s5;
        } else {
            s5 = "<html>Não é possível realizar a entrega</html>";
            return s5;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField distancia;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField peso;
    private javax.swing.JTextField tmaximo;
    // End of variables declaration//GEN-END:variables
}
