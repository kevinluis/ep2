package app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public final class Carro extends Veiculos {

    private int Numero_de_carros;

    private float VelocidadeMedia;

    private float PesoMaximo;

    public Carro() {
        setVelocidadeMedia(100);
        setPesoMaximo(360);
    }

    public float getVelocidadeMedia() {
        return VelocidadeMedia;
    }

    public void setVelocidadeMedia(float VelocidadeMedia) {
        this.VelocidadeMedia = VelocidadeMedia;
    }

    public float getPesoMaximo() {
        return PesoMaximo;
    }

    public void setPesoMaximo(float PesoMaximo) {
        this.PesoMaximo = PesoMaximo;
    }

    public void setNumero_de_carros(int Numero_de_carros) {
        this.Numero_de_carros = Numero_de_carros;
    }

    public int getNumero_de_carros() {
        return Numero_de_carros;
    }

    @Override
    public float Calcula_lucrot_gasolina(float Peso, float km) {
        Veiculos v = new Veiculos();
        v.setMargemLucro(v.lerLucro());
        float Rendimento = (float) ((float) 14.0 - (Peso * 0.025));
        float taxaLucro = (float) ((v.getMargemLucro() / 100) + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 4.449 * taxaLucro);
        return lucro;
    }

    @Override
    public float Calcula_lucrot_alcool(float Peso, float km) {
        Veiculos v = new Veiculos();
        v.setMargemLucro(v.lerLucro());
        float Rendimento = (float) ((float) 12.0 - (Peso * 0.0231));
        float taxaLucro = (float) ((v.getMargemLucro() / 100) + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 3.499 * taxaLucro);
        return lucro;
    }

    @Override
    public float Tempo_de_viagem(float Km) {
        float Tempo_de_viagem = Km / getVelocidadeMedia();
        return Tempo_de_viagem;
    }

    public int nCarros() {
        Carro c = new Carro();
        int n;
        try {
            FileInputStream arquivo = new FileInputStream("src/app/carro.txt");
            InputStreamReader input = new InputStreamReader(arquivo);
            BufferedReader br = new BufferedReader(input);
            String linha;
            do {
                linha = br.readLine();
                if (linha != null) {
                    c.setNumero_de_carros(Integer.parseInt(linha));
                }
            } while (linha != null);
        } catch (Exception e) {
            System.out.println("error");
        }
        n = c.getNumero_de_carros();
        return n;
    }

    public void alterarNCarros(int n) {
        String N = String.valueOf(n);
        try {
            FileOutputStream arquivo = new FileOutputStream("src/app/carro.txt");
            PrintWriter pr = new PrintWriter(arquivo);
            pr.println(N);
            pr.close();
            arquivo.close();
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }

    @Override
    public boolean RealizaEntrega(float peso, float tmaximo, float distancia, int quantidadeVeiculos) {
        if (Tempo_de_viagem(distancia) <= tmaximo && peso <= getPesoMaximo() && 0 < quantidadeVeiculos) {
            return true;
        } else {
            return false;
        }
    }

    public String MostraConfirmacao() {
        String s1 = "Voce Selecionou um carro";
        return s1;
    }
}
