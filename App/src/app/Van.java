package app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public final class Van extends Veiculos {

    private int Numero_de_vans;

    private float VelocidadeMedia;

    private float PesoMaximo;

    public Van() {
        setVelocidadeMedia(80);
        setPesoMaximo(3500);
    }

    public float getVelocidadeMedia() {
        return VelocidadeMedia;
    }

    public void setVelocidadeMedia(float VelocidadeMedia) {
        this.VelocidadeMedia = VelocidadeMedia;
    }

    public float getPesoMaximo() {
        return PesoMaximo;
    }

    public void setPesoMaximo(float PesoMaximo) {
        this.PesoMaximo = PesoMaximo;
    }

    public void setNumero_de_vans(int Numero_de_vans) {
        this.Numero_de_vans = Numero_de_vans;
    }

    public int getNumero_de_vans() {
        return Numero_de_vans;
    }

    @Override
    public float Calcula_lucrot_diesel(float Peso, float Km) {
        Veiculos v = new Veiculos();
        v.setMargemLucro(v.lerLucro());
        float Rendimento = (float) ((float) 10.0 - (Peso * 0.001));
        float taxaLucro = (float) ((v.getMargemLucro() / 100) + 1.0);
        float lucro = (float) ((float) (Km / Rendimento) * 3.869 * taxaLucro);
        return lucro;
    }

    @Override
    public float Tempo_de_viagem(float Km) {
        float Tempo_de_viagem = Km / getVelocidadeMedia();
        return Tempo_de_viagem;
    }

    public int nVans() {
        Van v = new Van();
        int n;
        try {
            FileInputStream arquivo = new FileInputStream("src/app/van.txt");
            InputStreamReader input = new InputStreamReader(arquivo);
            BufferedReader br = new BufferedReader(input);
            String linha;
            do {
                linha = br.readLine();
                if (linha != null) {
                    v.setNumero_de_vans(Integer.parseInt(linha));
                }
            } while (linha != null);
        } catch (Exception e) {
            System.out.println("error");
        }
        n = v.getNumero_de_vans();
        return n;
    }

    public void alterarNVans(int n) {
        String N = String.valueOf(n);
        try {
            FileOutputStream arquivo = new FileOutputStream("src/app/van.txt");
            PrintWriter pr = new PrintWriter(arquivo);
            pr.println(N);
            pr.close();
            arquivo.close();
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }

    @Override
    public boolean RealizaEntrega(float peso, float tmaximo, float distancia, int quantidadeVeiculos) {
        if (Tempo_de_viagem(distancia) <= tmaximo && peso <= getPesoMaximo() && 0 < quantidadeVeiculos) {
            return true;
        } else {
            return false;
        }
    }

    public String MostraConfirmacao() {
        String s1 = "Voce Selecionou uma van";
        return s1;
    }
}
