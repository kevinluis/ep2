package app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Veiculos {

    private float MargemLucro;

    public Veiculos() {
    }

    public float Calcula_lucrot_diesel(float Peso, float km) {
        float Rendimento = 0;
        float taxaLucro = (float) (getMargemLucro() + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 3.869 * taxaLucro);
        return lucro;
    }

    public float Calcula_lucrot_gasolina(float Peso, float km) {
        float Rendimento = 0;
        float taxaLucro = (float) (getMargemLucro() + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 4.449 * taxaLucro);
        return lucro;
    }

    public float Calcula_lucrot_alcool(float Peso, float km) {
        float Rendimento = 0;
        float taxaLucro = (float) (getMargemLucro() + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 3.499 * taxaLucro);
        return lucro;
    }

    public float Tempo_de_viagem(float Km) {
        float Tempo_de_viagem = 0;
        return Tempo_de_viagem;
    }

    public float getMargemLucro() {
        return MargemLucro;
    }

    public void setMargemLucro(float MargemLucro) {
        this.MargemLucro = MargemLucro;
    }

    public float lerLucro() {
        Veiculos a1 = new Veiculos();
        float lucro;
        try {
            FileInputStream arquivo = new FileInputStream("src/app/lucro.txt");
            InputStreamReader input = new InputStreamReader(arquivo);
            BufferedReader br = new BufferedReader(input);
            String linha;
            do {
                linha = br.readLine();
                if (linha != null) {
                    a1.setMargemLucro(Float.parseFloat(linha));
                }
            } while (linha != null);
        } catch (Exception e) {
            System.out.println("error");
        }
        lucro = a1.getMargemLucro();
        return lucro;
    }

    public void alterarLucro(int lucro) {
        String lucroS = String.valueOf(lucro);
        try {
            FileOutputStream arquivo = new FileOutputStream("src/app/lucro.txt");
            PrintWriter pr = new PrintWriter(arquivo);
            pr.println(lucroS);
            pr.close();
            arquivo.close();
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }

    public boolean RealizaEntrega(float tmaximo, float peso, float distancia, int quantidadeVeiculos) {
        return true;
    }
}
