package app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public final class Moto extends Veiculos {

    private int Numero_de_motos;

    private float VelocidadeMedia;

    private float PesoMaximo;

    public Moto() {
        setVelocidadeMedia(110);
        setPesoMaximo(50);
    }

    public float getVelocidadeMedia() {
        return VelocidadeMedia;
    }

    public void setVelocidadeMedia(float VelocidadeMedia) {
        this.VelocidadeMedia = VelocidadeMedia;
    }

    public float getPesoMaximo() {
        return PesoMaximo;
    }

    public void setPesoMaximo(float PesoMaximo) {
        this.PesoMaximo = PesoMaximo;
    }

    @Override
    public float Calcula_lucrot_gasolina(float Peso, float km) {
        Veiculos v = new Veiculos();
        v.setMargemLucro(v.lerLucro());
        float Rendimento = (float) ((float) 50.0 - (Peso * 0.3));
        float taxaLucro = (float) ((v.getMargemLucro() / 100) + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 4.449 * taxaLucro);
        return lucro;
    }

    @Override
    public float Calcula_lucrot_alcool(float Peso, float km) {
        Veiculos v = new Veiculos();
        v.setMargemLucro(v.lerLucro());
        float Rendimento = (float) ((float) 43.0 - (Peso * 0.4));
        float taxaLucro = (float) ((v.getMargemLucro() / 100) + 1.0);
        float lucro = (float) ((float) (km / Rendimento) * 3.499 * taxaLucro);
        return lucro;
    }

    @Override
    public float Tempo_de_viagem(float Km) {
        float Tempo_de_viagem = Km / getVelocidadeMedia();
        return Tempo_de_viagem;
    }

    public int getNumero_de_motos() {
        return Numero_de_motos;
    }

    public void setNumero_de_motos(int Numero_de_motos) {
        this.Numero_de_motos = Numero_de_motos;
    }

    public int nMotos() {
        Moto m = new Moto();
        int n;
        try {
            FileInputStream arquivo = new FileInputStream("src/app/moto.txt");
            InputStreamReader input = new InputStreamReader(arquivo);
            BufferedReader br = new BufferedReader(input);
            String linha;
            do {
                linha = br.readLine();
                if (linha != null) {
                    m.setNumero_de_motos(Integer.parseInt(linha));
                }
            } while (linha != null);
        } catch (Exception e) {
            System.out.println("error");
        }
        n = m.getNumero_de_motos();
        return n;
    }

    public void alterarNMotos(int n) {
        String N = String.valueOf(n);
        try {
            FileOutputStream arquivo = new FileOutputStream("src/app/moto.txt");
            PrintWriter pr = new PrintWriter(arquivo);
            pr.println(N);
            pr.close();
            arquivo.close();
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }

    @Override
    public boolean RealizaEntrega(float peso, float tmaximo, float distancia, int quantidadeVeiculos) {
        if (Tempo_de_viagem(distancia) <= tmaximo && peso <= getPesoMaximo() && 0 < quantidadeVeiculos) {
            return true;
        } else {
            return false;
        }
    }

    public String MostraConfirmacao() {
        String s1 = "Voce Selecionou uma moto";
        return s1;
    }
}
