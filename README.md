# EP2 - KL Tranportadora

Um programa simples para a empresa calcular o melhor custo/beneficio, menor custo de operação e menor tempo de viagem de suas entregas.

## Registro de frota

<pr>A empresa começa sem veículos e para registrar a frota clique no botão "registro de frota". Nos campos de texto deve ser digitado a quantidade de cada veículo e clicar enter ou botão de ok. Essa frota persistirá mesmo com o fechamento do programa. <br />

## Alterar margem de lucro 

<pr>A margem de lucro no inicio é 0% e para mudá-la clique no botão "Alterar margem de lucro". Na janela aberta aparecerá a margem de lucro atual e um campo de texto em que deve ser digitada a desejada. 
Ex:
- 1 -> 1%
- 0.8 -> 0.8%
- 30 -> 30%
<br />

## Calcular frete

<pr>Após definir a margem de lucro e a quantidade de veículos da empresa, clique no botão "Calcular frete". Todos os campos de texto devem ser preenchidos e logo após o preenchimento clicar no botão calcular, isso mostrará as informações do veículo com: Melhor custo/beneficio, menor custo de operação e menor tempo de entrega, levando em conta os veículos disponíveis*. Há a opção de recalcular e de escolher cada veículo caso ele esteja disponivel e consiga realizar a entrega com as demandas definidas pelo usuário.<br />

*Vale ressaltar que ao fechar o programa, os veículos mandados para a entrega retornarão ao iniciar o aplicativo novamente. Para exemplificar, imagine que a empresa possui 4 carros e 2 deles foram mandados para entrega antes do fechamento do programa, ao iniciar o programa novamente esses 2 carros estarão disponiveis novamente.

## Funcionamento

<pr>É de suma importância os arquivos "lucro.txt","moto.txt","van.txt","carreta.txt" e "carro.txt" estarem no caminho: src/app. Afetará diretamente no funcionamento do aplicativo caso não estejam.

<pr>Todos os campos de texto da janela "Calcular frete" devem ser preenchidos antes de se clicar em calcular. 

